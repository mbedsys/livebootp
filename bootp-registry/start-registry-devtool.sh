#!/bin/sh -e

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\e[91m[CRIT] %s\e[0m\n" "$@"
    exit 1
}

# Print an info message
print_info() {
    printf "\e[92m[INFO] %s\e[0m\n" "$@"
}

# Print a warning message
print_warning() {
    >&2 printf "\e[93m[WARN] %s\e[0m\n" "$@"
}

IMAGE_NAME=$1
LIVEBOOTP_PROJECT_DIR=$(readlink -f $(dirname $0)/..)

if [ -z "$IMAGE_NAME" ]; then
    print_critical "Use $0: <bootp-registry image name>"
fi

if [ ! -f "$LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config.yml" ]; then
    cp "$LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config-test.yml" "$LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config.yml"
    print_warning "No $LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config.yml found" \
                "  A new $LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config.yml configuration file will be generated using $LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config-test.yml" \
                "> You will have to edit the file $LIVEBOOTP_PROJECT_DIR/bootp-registry/config/config.yml and update:" \
                "  * The 'hardware-ethernet' property with the test host MAC adress" \
                "  * The 'image-name' property with the image/version to test"
fi

if ! docker network inspect livebootp > /dev/null 2>&1; then
    if ip -4 route | grep -q ^192.168.250; then
        print_critical "Subnet 192.168.250.0/24 seems to be already used, please edit this script to choose an other one"
    fi
    docker network create --driver bridge --subnet=192.168.250.0/24 \
        -o com.docker.network.bridge.name=br_livebootp livebootp
fi

print_info "Starting bootp registry..." \
    " => You have to use the bridge br_livebootp" \
    "type 'docker exec bootp-registry /opt/livebootp/bootp-registry-ctl.sh reload' to reload the configuration" \
    "> You can also setup a qemu guest with libvirt simply by running this command:
$ virt-install --connect qemu:///system --name my-target --vcpus 1 --memory 1024 \\
    --pxe --disk size=10 --boot network --os-variant ubuntu16.04 \\
    --network bridge=br_livebootp,model=virtio,mac=52:54:00:12:34:56 \\
    --noreboot --noautoconsole --wait 0"
set -x
exec docker run --name bootp-registry --rm -ti --tmpfs /var/run \
    -v $LIVEBOOTP_PROJECT_DIR/bootp-registry/config:/opt/livebootp/etc \
    -v $LIVEBOOTP_PROJECT_DIR/artifacts:/var/lib/tftpboot/images \
    -v $HOME/.ssh/id_rsa.pub:/var/lib/tftpboot/ssh_authorized_keys/default \
    --network livebootp "$IMAGE_NAME"
