# Cangelog

## 2.0.0
* [start-registry-devtool] Set bridge name to br_livebootp and fix subnet to 192.168.250.0/24
* [bootp-registry-ctl] Reworking globally the startup script
* [CI] Update project namespace, get ci-toolbox out and replace it by run-job-script.sh
* [CI] Add deploy job
* [CI/tools] Add livebootp-skip-build-job option for *-mksquashfs jobs
* [CI/Toolbox] Update to the version 4.1.0
* [bootp-registry] Add login username attribute into config.yml
* [bootp-registry] Add syslinux SYSAPPEND into pxelinux.cfg files
* [bootp-registry] Fix cmdline kernel args read from image
* [bootp-registry] Set hardware address to lower case
* [bootp-registry] Fix rsyslogd pid check during start process
* [CI] Add docker tag to the jobs

## 1.0.1
* [bootp-registry] Fix rsyslogd pid check during start process

## 1.0.0
* Initial version
