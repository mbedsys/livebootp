#!/bin/bash -e

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\e[91m[CRIT] %s\e[0m\n" "$@"
    exit 1
}

# Print an error message
print_error() {
    >&2 printf "\e[91m[ERRO] %s\e[0m\n" "$@"
}

# Print a warning message
print_warning() {
    >&2 printf "\e[93m[WARN] %s\e[0m\n" "$@"
}

# Print a note message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Print an info message
print_info() {
    printf "\e[92m[INFO] %s\e[0m\n" "$@"
}

cdr2mask() {
   # Number of args to shift, 255..255, first non-255 byte, zeroes
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [ $1 -gt 1 ] && shift $1 || shift
   echo ${1-0}.${2-0}.${3-0}.${4-0}
}

netaddr() {
    IFS=. read -r i1 i2 i3 i4 <<< $1
    IFS=. read -r m1 m2 m3 m4 <<< $2
    printf "%d.%d.%d.%d\n" "$((i1 & m1))" "$((i2 & m2))" "$((i3 & m3))" "$((i4 & m4))"
}

br_read_global_config() {
    eval "$(br_config_query -r '."bootp-registry" // {} | "
        LIVEBOOTP_DNS=(\(@sh"\(."domain-name-servers" // ["8.8.8.8", "8.8.4.4"]
            | select (. != "::1" and .[0:3] != "127")))")
        LIVEBOOTP_DN=\(@sh"\(."domain-name" // "example.com")")
        LIVEBOOTP_CN=\(@sh"\(."targets-domain-name" // "")")
        LIVEBOOTP_INTF=\(@sh"\(."listen-intf"
            // "'"$(ip -4 route show | awk '/^default / {print $5; exit;}')"'")")
        LIVEBOOTP_ROUTERS=(\(@sh"\(."routers" // 
            ["'"$(ip -4 route show | awk '/^default / {print $3; exit;}')"'"])"))
        LIVEBOOTP_ADDR=\(@sh"\(.address // "")")
        LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL=\(@sh"\(."ssh-authorized-keys-base-url" // "")")
        LIVEBOOTP_SQUASHFS_REPO_URL=\(@sh"\(."image-repo-url" // "")")
        LIVEBOOTP_SYSAPPEND_DEFAULT=\(@sh"\(."sysappend-default" // "0x18032")")"
    ')"
    print_note "DNS list                           : ${LIVEBOOTP_DNS[*]}"
    print_note "Root domain name                   : $LIVEBOOTP_DN"
    if [ -z "$LIVEBOOTP_CN" ]; then
        LIVEBOOTP_CN="bootp-registry.$LIVEBOOTP_DN"
    fi
    print_note "Registry domain name               : $LIVEBOOTP_CN"
    print_note "Listen on interface                : $LIVEBOOTP_INTF"
    print_note "Using routers                      : ${LIVEBOOTP_ROUTERS[*]}"
    if [ -z "$LIVEBOOTP_ADDR" ]; then
        LIVEBOOTP_ADDR="$(ip -4 addr show dev $LIVEBOOTP_INTF | awk '/^    inet / { print $2}')"
    fi
    print_note "Server adress                      : $LIVEBOOTP_ADDR"
    LIVEBOOTP_NETMASK=$(cdr2mask ${LIVEBOOTP_ADDR#*/})
    print_note "Server netmask                     : $LIVEBOOTP_NETMASK"
    LIVEBOOTP_IP=${LIVEBOOTP_ADDR%/*}
    LIVEBOOTP_NETADDR=$(netaddr $LIVEBOOTP_IP $LIVEBOOTP_NETMASK)
    print_note "Network adress                     : $LIVEBOOTP_NETADDR"
    if [ -z "$LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL" ]; then
        LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL="http://$LIVEBOOTP_IP/ssh_authorized_keys"
    fi
    print_note "SSH authorized keys base URL       : $LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL"
    if [ -z "$LIVEBOOTP_SQUASHFS_REPO_URL" ]; then
        LIVEBOOTP_SQUASHFS_REPO_URL="http://$LIVEBOOTP_IP/images"
    fi
    print_note "SquashFS Image repo URL            : $LIVEBOOTP_SQUASHFS_REPO_URL"
    print_note "SYSAPPEND default value            : $LIVEBOOTP_SYSAPPEND_DEFAULT"
    LIVEBOOTP_DHCPD_CONF="/etc/dhcp/dhcpd.conf"
    LIVEBOOTP_TFTPROOT="/var/lib/tftpboot"
    LIVEBOOTP_IMAGES_DIR="$LIVEBOOTP_TFTPROOT/images"
    LIVEBOOTP_SSH_HOST_CONFIG="$LIVEBOOTP_TFTPROOT/ssh_host_config.txt"
    LIVEBOOTP_PXELINCFG="$LIVEBOOTP_TFTPROOT/pxelinux.cfg"
}

br_add_host() {
    local host=$1
    local hwaddr=${2,,} # Set to lowercase
    local address=$3
    local image=$4
    local login_user=$5
    local ssh_auth_keys=$6
    local cmdline_append=$7
    local nopersistence=$8
    local sysappend=$9
    print_note "Add host $host..."

    # Write DHCP & SSH part
    print_note " * HW address                      : $hwaddr"
    print_note " * Fixed address                   : $address"
    if [ -z "$hwaddr" ] || [ -z "$address" ]; then
        test [ -n "$hwaddr" ] || print_error "Missing hardware-ethernet property"
        test [ -n "$address" ] || print_error "Missing fixed-address property"
        print_warning "Skipping this host due to previous errors"
        return
    fi
    printf "%s\n"                            \
        "    host $host {"                   \
        "        hardware ethernet $hwaddr;" \
        "        fixed-address $address;"    \
        "    }" >> $LIVEBOOTP_DHCPD_CONF
    printf "%s\n"                            \
        "Host $host"                         \
        "HostName $address"                  \
        "User root" "" >> "$LIVEBOOTP_SSH_HOST_CONFIG"

    # Write BOOTP part
    if [ -z "$image" ]; then
        print_note " * No image-name property found, skipping bootp part for this host..."
        return
    fi
    print_note " * Image name                      : $image"
    if [[ "$image" =~ $URL_PATTERN ]]; then
        image_url="$image"
    else
        image_url="$LIVEBOOTP_SQUASHFS_REPO_URL/$image"
    fi
    print_note " * SSH login username              : $login_user"
    if [[ "$ssh_auth_keys" =~ $URL_PATTERN ]]; then
        ssh_auth_keys_path="$ssh_auth_keys"
    else
        ssh_auth_keys_path="$LIVEBOOTP_SSH_AUTHORIZED_KEYS_BASE_URL/$ssh_auth_keys"
    fi
    print_note " * SSH authorized keys file        : $ssh_auth_keys"
    print_note " * SYSAPPEND value                 : $sysappend"
    # Compute APPEND line
    local bootp_append_elts=(
        initrd=images/$image/initrd.img
        boot=live
        dhcp
        hostname=$host
        fetch=$image_url/rootfs.squashfs
        fetch-ssh-authorized-keys=$ssh_auth_keys_path
    )
    if [ "$nopersistence" != "true" ]; then
        bootp_append_elts+=(persistence)
    fi
    bootp_append_elts+=($cmdline_append)
    if [ -f "$LIVEBOOTP_IMAGES_DIR/images/$image/cmdline" ]; then
        bootp_append_elts+=($(cat "$LIVEBOOTP_IMAGES_DIR/images/$image/cmdline"))
    fi
    print_note " * Command line append             : ${bootp_append_elts[*]}"
    # Write the file
    mkdir -p $LIVEBOOTP_TFTPROOT/pxelinux.cfg
    printf "%s\n" \
        "UI menu.c32" \
        "PROMPT 0" "" \
        "MENU TITLE Boot Menu" \
        "TIMEOUT 10" \
        "DEFAULT liveboot" "" \
        "MENU" \
        "    LABEL Live boot image $image" \
        "    LINUX images/$image/vmlinuz" \
        "    APPEND ${bootp_append_elts[*]}" \
        "    SYSAPPEND $sysappend" \
        "" > "$LIVEBOOTP_TFTPROOT/pxelinux.cfg/01-${hwaddr//:/-}"
}

br_write_dhcp_config() {
    printf "%s\n"                                                               \
        "ddns-update-style none;"                                               \
        "option domain-name \"$LIVEBOOTP_DN\";"                                 \
        "option domain-name-servers $(tr ' ' ',' <<< "${LIVEBOOTP_DNS[*]}");"   \
        "default-lease-time 3600;"                                              \
        "max-lease-time 86400;"                                                 \
        "authoritative;"                                                        \
        "log-facility local7;"                                                  \
        "subnet $LIVEBOOTP_NETADDR netmask $LIVEBOOTP_NETMASK {"                \
        "    option routers $(tr ' ' ',' <<< "${LIVEBOOTP_ROUTERS[*]}");"       \
        "    next-server $LIVEBOOTP_IP;"                                        \
        "    filename \"pxelinux.0\";"                                          \
        "" > $LIVEBOOTP_DHCPD_CONF
    eval "$(br_config_query -r '. | to_entries[]
        | select(.key[0:1] != "." and (.value | has("hardware-ethernet")))
        | "br_add_host \(@sh"\(.key)")"
            + " \(@sh"\(.value."hardware-ethernet" // "")")"
            + " \(@sh"\(.value."fixed-address" // "")")"
            + " \(@sh"\(.value."image-name" // "")")"
            + " \(@sh"\(.value."login-username" // "root")")"
            + " \(@sh"\(.value."ssh-authorized-keys" // "default")")"
            + " \(@sh"\(.value."cmdline-append" // "")")"
            + " \(@sh"\(.value."nopersistence" // "")")"
            + " \(@sh"\(.value."sysappend" // "'"$LIVEBOOTP_SYSAPPEND_DEFAULT"'")")"')"
    echo "}" >> $LIVEBOOTP_DHCPD_CONF
}

if [ ! -f /opt/livebootp/etc/config.yml ]; then
    print_critical "File /opt/livebootp/etc/config.yml not found"
fi

# YAML to JSON conversion
LIVEBOOTP_CONFIG="$(cat /opt/livebootp/etc/config.yml \
    | python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout)')"
br_config_query() {
    jq "$@" <<< "$LIVEBOOTP_CONFIG"
}
print_info "Read configuration file: /opt/livebootp/etc/config.yml"

br_read_global_config

# Write NGinx configuration
cat > /etc/nginx/conf.d/default.conf <<EOF
server {
        listen $LIVEBOOTP_IP:80;

        root /var/lib/tftpboot;
        index index.html;

        server_name $LIVEBOOTP_IP $LIVEBOOTP_CN;

        location / {
                autoindex on;
                try_files \$uri \$uri/ =404;
        }
}
EOF

URL_PATTERN='^\w+://'
> "$LIVEBOOTP_SSH_HOST_CONFIG"

br_write_dhcp_config

# DHCP conf sanity check
if ! /usr/sbin/dhcpd -t -q -cf "$LIVEBOOTP_DHCPD_CONF" > /dev/null 2>&1; then
    cat $LIVEBOOTP_DHCPD_CONF
    print_critical "dhcpd self-test failed. Please fix $LIVEBOOTP_DHCPD_CONF." \
                "The error was: $(/usr/sbin/dhcpd -t -cf "$LIVEBOOTP_DHCPD_CONF" 2>&1)"
fi
touch /var/lib/dhcp/dhcpd.leases

# Check NGinx configuration
mkdir -p /run/nginx
if ! /usr/sbin/nginx -t -c /etc/nginx/nginx.conf > /dev/null 2>&1; then
    cat /etc/nginx/nginx.conf
    print_critical "nginx self-test failed. Please fix /etc/nginx/nginx.conf" \
                "The error was: $(/usr/sbin/nginx -t -c /etc/nginx/nginx.conf 2>&1)"
fi

print_info "Read configuration file [DONE]"

LIVEBOOTP_DHCPD_PID="/run/dhcpd.pid"
case "$1" in
    start)
        if [ "$(cat /proc/$(cat /run/rsyslogd.pid)/comm 2> /dev/null)" == "rsyslogd" ]; then
            print_critical "Already started, please use rotate/reload action, otherwise restart docker instance"
        fi
        start-stop-daemon --start --quiet --pidfile /run/rsyslogd.pid \
                --exec /usr/sbin/rsyslogd --
        start-stop-daemon --start --quiet --pidfile /run/nginx/nginx.pid \
                --exec /usr/sbin/nginx -- -c /etc/nginx/nginx.conf
        start-stop-daemon --start --quiet --exec /usr/sbin/in.tftpd -- \
                --listen --user tftp --address $LIVEBOOTP_IP:69 \
                --secure $LIVEBOOTP_TFTPROOT
        start-stop-daemon --start --quiet --pidfile "$LIVEBOOTP_DHCPD_PID" \
                --exec /usr/sbin/dhcpd -- \
                -cf "$LIVEBOOTP_DHCPD_CONF" -pf "$LIVEBOOTP_DHCPD_PID" $LIVEBOOTP_INTF
        while [ -e /run/rsyslogd.pid ]; do
            tail -f /var/log/messages
        done
        ;;
    rotate)
        start-stop-daemon --stop --signal HUP --quiet --pidfile /run/rsyslogd.pid \
                --exec /usr/sbin/rsyslogd
        ;;
    reload)
        start-stop-daemon --stop --quiet --pidfile "$LIVEBOOTP_DHCPD_PID"
        sleep 2
        start-stop-daemon --start --quiet --pidfile "$LIVEBOOTP_DHCPD_PID" \
                --exec /usr/sbin/dhcpd -- \
                -cf "$LIVEBOOTP_DHCPD_CONF" -pf "$LIVEBOOTP_DHCPD_PID" $LIVEBOOTP_INTF
        ;;
esac
