# Cangelog

## 2.0.0
* [CI] Update project namespace, get ci-toolbox out and replace it by run-job-script.sh
* [CI] Add deploy job
* [CI/tools] Add livebootp-skip-build-job option for *-mksquashfs jobs
* [CI/Toolbox] Update to the version 4.1.0
* [targets/base] Add support of debian based images and remove TFTP protocol support for squashfs image fetch
* [targets/base] reorganize the live-boot patch folder and add network-add-source-directory.patch
* [targets/base] Add support of nopersistence live-boot parameter (see man live-boot)
* [targets/base] Add tag (file /.live-build) to force update-initramfs
* [targets] Fix the typo libeboot => liveboot
* [CI] Add docker tag to the jobs

## 1.2.0
* [target/build] Add target_docker_build_* hooks
* [target/base] Add FROM_UBUNTU_VERSION Dockerfile argument

## 1.1.0
* [target/base] Remove Linux kernel image installation to be able to install a custom image on the final image targets

## 1.0.0
* Initial version
