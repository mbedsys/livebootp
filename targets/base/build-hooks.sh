
target_docker_build_before() {
    local from_distrib_version distrib_name distrib_specific_pkg_list
    case "$LIVEBOOTP_TARGET_IMAGE_VERSION" in
        *-ubuntu-*)
            distrib_name="ubuntu"
            distrib_specific_pkg_list="ubuntu-standard"
        ;;
        *-debian-*)
            distrib_name="debian"
        ;;
        *)
            print_critical "Unable to detect debian like distribution name from the given tag: $LIVEBOOTP_TARGET_IMAGE_VERSION"
        ;;
    esac
    local pattern='^.*-'"$distrib_name"'-.*$'
    if [[ $LIVEBOOTP_TARGET_IMAGE_VERSION =~ $pattern ]]; then
        local from_distrib_version=${LIVEBOOTP_TARGET_IMAGE_VERSION##*-$distrib_name-}
        LIVEBOOTP_DOCKER_BUILD_ARGS+=(--build-arg "FROM_DISTRIB=$distrib_name:$from_distrib_version"
                                      --build-arg "DISTRIB_SPECIFIC_PKG_LIST=$distrib_specific_pkg_list")
        print_note "Image based on docker image $distrib_name:$from_distrib_version"
    fi
}
