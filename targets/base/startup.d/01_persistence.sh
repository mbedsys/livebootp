
if [ "${KERNEL_CMDLINE_PARAMS[persistence]}" == "true" ] \
    && [ "${KERNEL_CMDLINE_PARAMS[nopersistence]}" != "true" ]; then
    LIVEBOOTP_PERSISTENCE_ENABLED="true"
fi
LIVEBOOTP_PERSISTENCE_LABEL=${KERNEL_CMDLINE_PARAMS[persistence-label]:-persistence}

if [ "$LIVEBOOTP_PERSISTENCE_ENABLED" == "true" ]; then
    # Setup persistence
    if [ -e /dev/disk/by-label/$LIVEBOOTP_PERSISTENCE_LABEL ]; then
        PERSISTENCE_ROOT="/lib/live/mount/persistence/$(basename $( \
            readlink -f /dev/disk/by-label/$LIVEBOOTP_PERSISTENCE_LABEL))"
        if [ ! -d "$PERSISTENCE_ROOT" ]; then
            mkdir -p $PERSISTENCE_ROOT
            mount /dev/disk/by-label/$LIVEBOOTP_PERSISTENCE_LABEL $PERSISTENCE_ROOT
        fi
    else
        # get the first available hard drive
        KNAME="$(lsblk -io TYPE,KNAME | awk '/^disk / {print $2; exit;}')"
        if [ -z "$KNAME" ]; then
            print_warning "No disk found for persistence - Persistence mode disabled"
            LIVEBOOTP_PERSISTENCE_ENABLE="false"
        else
            DISK_DEV="/dev/$KNAME"
            # To be sure that the MBR is erased
            dd if=/dev/zero of=$DISK_DEV bs=512 count=1 conv=sync
            # Set a new partition table
            parted $DISK_DEV mklabel gpt Y
            # Create a new partition on the whole disk
            parted $DISK_DEV mkpart primary 0% 100%
            # Get the first part dev path
            for i in $(seq 1 10); do
                KPART=$(lsblk -io TYPE,KNAME $DISK_DEV | awk '/^part / {print $2; exit;}')
                if [ -n "$KPART" ]; then
                    break
                fi
                sleep 1
            done
            PART_DEV="/dev/$KPART"
            # Formate to ext4 + label=$LIVEBOOTP_PERSISTENCE_LABEL
            yes | mkfs.ext4 -L $LIVEBOOTP_PERSISTENCE_LABEL $PART_DEV
            PERSISTENCE_ROOT="/lib/live/mount/persistence/$(basename $PART_DEV)"
            # mount persistence storage
            mkdir -p "$PERSISTENCE_ROOT"
            mount $PART_DEV "$PERSISTENCE_ROOT"
        fi
    fi
fi

if [ "$LIVEBOOTP_PERSISTENCE_ENABLED" == "true" ]; then
    PERSISTENCE_CONFIG="$PERSISTENCE_ROOT/persistence.conf"
    > "$PERSISTENCE_CONFIG"

    # Add pertistant s
    add_pertistant_storage() {
        local target="${1:1}"
        local method="$2"
        if ! is_mounted "/$target" && ! [ -L "/$target" ]; then
            case "$method" in
                link)
                    mkdir -p "$(dirname "$PERSISTENCE_ROOT/$target")"
                    touch "$PERSISTENCE_ROOT/$target"
                    ln -sf "$PERSISTENCE_ROOT/$target" "/$target"
                    ;;
                union)
                    mkdir -p "$PERSISTENCE_ROOT/$target/"{rw,work}
                    mount -t overlay overlay -o "rw,noatime,lowerdir=/lib/live/mount/rootfs/rootfs.squashfs/$target,upperdir=$PERSISTENCE_ROOT/$target/rw,workdir=$PERSISTENCE_ROOT/$target/work" "/$target"
                    ;;
                ''|bind)
                    method="bind"
                    case "$3" in
                        none)
                            test -e "$PERSISTENCE_ROOT/$target" || mkdir -p "$PERSISTENCE_ROOT/$target"
                            ;;
                        ''|copy)
                            local pdir="$(dirname "$PERSISTENCE_ROOT/$target")"
                            mkdir -p "$pdir"
                            test -e "$PERSISTENCE_ROOT/$target" || cp -a "/$target" "$pdir/"
                            ;;
                        *)
                            add_pertistant_storage_bind_init_$3 "$PERSISTENCE_ROOT/$target" "/$target"
                            ;;
                    esac
                    mount -o bind "$PERSISTENCE_ROOT/$target" "/$target"
                    ;;
                *)
                    print_critical "add_pertistant_storage: Unmanaged method $method"
                    ;;
            esac
        fi
        echo "/$target $method" >> "$PERSISTENCE_CONFIG"
    }
else
    add_pertistant_storage() {
        print_warning "add_pertistant_storage: Persistence mode disabled"
    }
fi
