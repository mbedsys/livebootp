
citbx_use "dockerimg"

target_build_exec_hook() {
    local hook_function=$1
    shift
    if [ -f "targets/$LIVEBOOTP_TARGET_IMAGE_NAME/build-hooks.sh" ]; then
        cd targets/$LIVEBOOTP_TARGET_IMAGE_NAME
        source "build-hooks.sh"
        if [[ "$(type -t $hook_function)" == "function" ]]; then
            $hook_function "$@"
        else
            print_note "HOOK[$hook_function]: Function $hook_function not found, skipping this hook"
        fi
    else
        print_note "HOOK[$hook_function]: File targets/$LIVEBOOTP_TARGET_IMAGE_NAME/build-hooks.sh not found, skipping this hook"
        return
    fi
    cd "$CI_PROJECT_DIR"
}

job_setup() {
    # Only on workstation, set the suitable tag prefix if not set
    local job_prefix=${CI_JOB_NAME%-build}
    CI_COMMIT_TAG="$job_prefix/${CI_COMMIT_TAG#$job_prefix/}"
}

job_main() {
    local pattern='^target-.*-build$'
    if ! [[ $CI_JOB_NAME =~ $pattern ]]; then
        print_critical "Target image job name must begin by 'target-' and terminate by '-build'"
    fi
    LIVEBOOTP_TARGET_IMAGE_NAME=${CI_JOB_NAME%-build}
    pattern='^'"$LIVEBOOTP_TARGET_IMAGE_NAME"'/.*$'
    if ! [[ $CI_COMMIT_TAG =~ $pattern ]]; then
        print_critical "This job cannot be launched with the following tag '$CI_COMMIT_TAG'" \
                        "This error is probably due to:" \
                        " * on a Gitlab runner: Missing or incorrect 'only' tag in the .gitlab-ci.yml - You can put this one:" \
                        "    only:" \
                        "        - /^$LIVEBOOTP_TARGET_IMAGE_NAME\/.*\$/" \
                        " * on your local worspace: You have launched ci-toolbox $CI_JOB_NAME with the wrong tag - Try with the additional option:" \
                        "    --image-tag $LIVEBOOTP_TARGET_IMAGE_NAME/x.y.z"
    fi
    LIVEBOOTP_TARGET_IMAGE_NAME=${LIVEBOOTP_TARGET_IMAGE_NAME#target-}
    LIVEBOOTP_TARGET_IMAGE_VERSION=${CI_COMMIT_TAG#${CI_JOB_NAME%-build}/}
    LIVEBOOTP_TARGET_IMAGE_TAG="$CI_REGISTRY_IMAGE/$LIVEBOOTP_TARGET_IMAGE_NAME:$LIVEBOOTP_TARGET_IMAGE_VERSION"
    # Build docker image
    if [ "$LIVEBOOTP_TARGET_IMAGE_NAME" != "base" ]; then
        LIVEBOOTP_DOCKER_BUILD_ARGS+=(--build-arg "CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE")
    fi
    target_build_exec_hook target_docker_build_before
    LIVEBOOTP_DOCKER_BUILD_ARGS+=(-t "$LIVEBOOTP_TARGET_IMAGE_TAG")
    docker build "${LIVEBOOTP_DOCKER_BUILD_ARGS[@]}" \
        "targets/$LIVEBOOTP_TARGET_IMAGE_NAME/"
    if [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push "$LIVEBOOTP_TARGET_IMAGE_TAG"
    fi
}

job_after() {
    local retcode=$1
    target_build_exec_hook target_docker_build_after $1
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$LIVEBOOTP_TARGET_IMAGE_TAG\" successfully generated"
    fi
}
