
job_define() {
    bashopts_declare -n LIVEBOOTP_REGISTRIES -l livebootp-registries \
        -d "LiveBOOTP registry host list ('host1 host2 etc.')" -t string -r -s
    bashopts_declare -n LIVEBOOTP_SSH_RSA_PRIVATE_KEY -l livebootp-ssh-rsa-private-key \
        -d "LiveBOOTP SSH private key to connect to the registries" -t string -s
    bashopts_declare -n LIVEBOOTP_SSH_DSA_PRIVATE_KEY -l livebootp-ssh-dsa-private-key \
        -d "LiveBOOTP SSH private key to connect to the registries" -t string -s
    bashopts_declare -n LIVEBOOTP_SSH_ECDSA_PRIVATE_KEY -l livebootp-ssh-ecdsa-private-key \
        -d "LiveBOOTP SSH private key to connect to the registries" -t string -s
    bashopts_declare -n LIVEBOOTP_SSH_ED25519_PRIVATE_KEY -l livebootp-ssh-ed25519-private-key \
        -d "LiveBOOTP SSH private key to connect to the registries" -t string -s
    citbx_export LIVEBOOTP_REGISTRIES LIVEBOOTP_SSH_RSA_PRIVATE_KEY \
        LIVEBOOTP_SSH_DSA_PRIVATE_KEY LIVEBOOTP_SSH_ECDSA_PRIVATE_KEY \
        LIVEBOOTP_SSH_ED25519_PRIVATE_KEY
}

job_setup() {
    if [ -z "$LIVEBOOTP_SSH_RSA_PRIVATE_KEY" ] \
        && [ -z "$LIVEBOOTP_SSH_DSA_PRIVATE_KEY" ] \
        && [ -z "$LIVEBOOTP_SSH_ECDSA_PRIVATE_KEY" ] \
        && [ -z "$LIVEBOOTP_SSH_ED25519_PRIVATE_KEY" ]; then
        for type in rsa dsa ecdsa ed25519; do
            if [ -f $HOME/.ssh/id_$type ]; then
                citbx_docker_run_add_args -v "$HOME/.ssh/id_$type:$HOME/.ssh/id_$type" \
                    -v "$HOME/.ssh/id_$type:/root/.ssh/id_$type"
            fi
        done
    fi
}

job_main() {
    local target target_name target_version
    local image_dir=${LIVEBOOTP_REGISTRY_IMAGE_DIR:-/opt/bootp-registry/images}
    local remote_user=${LIVEBOOTP_REGISTRY_USER:-root}
    local remote_server
    local target_list=($(test ! -d artifacts \
        || find artifacts/ -mindepth 2 -maxdepth 2 -type d | grep -o '[^/]*/[^/]*$'))
    if [ ${#target_list[@]} -eq 0 ]; then
        print_info "Nothing to deploy, skipping this stage..."
        return 0
    fi
    if [ -z "$LIVEBOOTP_REGISTRIES" ]; then
        print_critical "Undeclared LIVEBOOTP_REGISTRIES. This environment variable must contains the LiveBOOTP registry host list"
    fi
    mkdir -p $HOME/.ssh
    sudo chown $(id -u):$(id -g) $HOME/.ssh
    chmod 700 $HOME/.ssh
    for type in rsa dsa ecdsa ed25519; do
        if eval "[ -n \"\$LIVEBOOTP_SSH_${type^^}_PRIVATE_KEY\" ]"; then
            eval "echo \"\$LIVEBOOTP_SSH_${type^^}_PRIVATE_KEY\" > $HOME/.ssh/id_$type"
            chmod 600 $HOME/.ssh/id_$type
        fi
    done
    if [ $(find $HOME/.ssh/ -regex '.*/id_\(rsa\|dsa\|ecdsa\|ed25519\)' | wc -l) -eq 0 ]; then
        print_critical "No SSH private key found into $HOME/.ssh" \
            "You may have to define one of LIVEBOOTP_SSH_(RSA|DSA|ECDSA|ED25519)_PRIVATE_KEY environment variable"
    fi
    for remote_server in $LIVEBOOTP_REGISTRIES; do
        print_info "Deploying onto server $remote_server..."
        ssh-keyscan -H $remote_server >> $HOME/.ssh/known_hosts
        # Remove old/outdated images
        # We consider that an image is outdated when the corresponding git tag doesn't exist
        local tag_list="$(git tag)"
        for target in $(ssh root@$remote_server find $image_dir -mindepth 2 -maxdepth 2 -type d \
            | grep -o '[^/]*/[^/]*$'); do
            pattern="\b$target\b"
            if ! [[ $tag_list =~ $pattern ]]; then
                print_info "Removing outdated target $target"
                ssh root@$remote_server rm -r $image_dir/$target
            fi
        done
        # Install target images from artifacts
        for target in "${target_list[@]}"; do
            print_info "Installing target $target..."
            target_name=${target%/*}
            target_version=${target#*/}
            # Remove directory if already exist
            ssh root@$remote_server "[ ! -d $image_dir/$target ] || rm -r $image_dir/$target"
            # Install target image into the destination folder
            ssh root@$remote_server mkdir -p $image_dir/$target_name
            scp -r artifacts/$target root@$remote_server:$image_dir/$target_name/
            ssh root@$remote_server chmod a+rX -R $image_dir/$target
        done
    done
}
